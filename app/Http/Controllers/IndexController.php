<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use DNS1D;
use App\Boleto;

class IndexController extends Controller
{
    
    public function result($tipo_boleto = null, $cuenta = false){
        
        if($cuenta){
            
            Boleto::createTableTmp($tipo_boleto, $cuenta);

            /*
            SELECT PeriCtct, BimeCtct, concepto.DetaConc, SaldCtct, RecaCtct, (SaldCtct+RecaCtct) Total  FROM tmp_ctacteboleto
            INNER JOIN concepto ON concepto.PeriInfo = tmp_ctacteboleto.PeriInfo
            AND concepto.CodiConc = tmp_ctacteboleto.CodiConc
            WHERE PeriCtct = 2018 AND SaldCtct > 0  AND CodiFapa = 0 AND NumeApre = 0
            */

            $boletos =  DB::table('tmp_ctacteboleto')
                        ->join('concepto', function($join){
                            $join->on('concepto.PeriInfo','=','tmp_ctacteboleto.PeriInfo');
                                    $join->on('concepto.CodiConc', '=' ,'tmp_ctacteboleto.CodiConc');
                        })
                        ->select('tmp_ctacteboleto.PeriCtct', 'tmp_ctacteboleto.BimeCtct', 'concepto.DetaConc', 'tmp_ctacteboleto.SaldCtct', 'tmp_ctacteboleto.RecaCtct', 'tmp_ctacteboleto.NumeCtct','tmp_ctacteboleto.MoviCtct','tmp_ctacteboleto.FeveCtct')
                        //->where('tmp_ctacteboleto.PeriCtct', '=', date('Y'))
                        ->where('tmp_ctacteboleto.SaldCtct', '>', 0)
                        ->where('tmp_ctacteboleto.CodiFapa', '=', 0)
                        ->where('tmp_ctacteboleto.NumeApre', '=', 0)
                        ->get();
        }else{
            $cuenta = "0000";
            $boletos = null;
            $tipo_boleto = "";
        }
        return view('index', array('cuenta'=>$cuenta, 'tipoboleto'=>$tipo_boleto ,'boletos'=>$boletos));
    }
    
    
    public function generarBoleto($cuenta, $tipo_bole, $bimestres){
        Boleto::createTableTmp($tipo_bole, $cuenta);
        
        $data = $this->insertarData($cuenta, $tipo_bole, $bimestres);
        $data['cuenta'] = $cuenta;
        $data['tipobole'] = $tipo_bole;
        $data['conceptos'] = Boleto::all_conceptos();
        $data['persona'] = Boleto::get_persona($cuenta,$tipo_bole);
        $data['inmueble'] = Boleto::get_inmueble($cuenta,$tipo_bole);
        $data['fechaactual'] = date('d/m/Y');
        
        //codigo de barras
        $codigo_brr = "55800";
        $codigo_brr .= str_pad($data['numeBole'], 7, "0", STR_PAD_LEFT); //numero de boleto
        $codigo_brr .= str_pad(explode("-", $bimestres)[0], 3, "0",STR_PAD_LEFT);//trae el primer bimestre seleccionado
        $codigo_brr .= Boleto::getTypeBoleto($tipo_bole).str_pad($cuenta, 7, "0",STR_PAD_LEFT); //ceunta
        $codigo_brr .= "8";//tipo de boleta
        $codigo_brr .= str_pad("1",2,"0",STR_PAD_LEFT);//periodo
        $codigo_brr .= str_pad(Boleto::getTypeBoleto($tipo_bole), 2, "0",STR_PAD_LEFT);//oficina
        $codigo_brr .= str_pad(strval(number_format($data['acu_total'],2,'','')), 9, "0",STR_PAD_LEFT);//importe
        $codigo_brr .= strval(date('ymd'));//vencimiento
        $codigo_brr .= "000000";//recargo
        $codigo_brr .= substr($this->codigoVerificadorXNET($codigo_brr, null), 1);//codigo verificador
        $data['codebar_str'] = $codigo_brr;
        //imprimo pdf
        $pdf = PDF::loadView('mipdf',$data);
        return $pdf->stream("boleto.pdf");
//        return view('mipdf',$data);
    }
    
    
    /*************** FUNCIONES PRIVADAS ****************/
    
    
    private function insertarData($cuenta, $tipo_bole, $periodos){
        
        $arr_peridos = str_replace("-",",", $periodos);
        
        $codiOfic = Boleto::get_oficina($tipo_bole);
        
        $boleto_i = Boleto::get_boleto($cuenta,$tipo_bole);
        
        $data['boleto'] = $boleto_i;
        
        $databoles_i = Boleto::get_data_bole($codiOfic, $cuenta, $arr_peridos);
        $data['databole'] = $databoles_i;
        
        $data['numeBole'] = DB::select(DB::raw("SELECT fnNumeBole(?) as numebole"), [date('Y')])[0]->numebole;
        $data['acu_dere'] = 0.00;
        $data['acu_reca'] = 0.00;
        foreach ($databoles_i as $row){
            $data['acu_dere'] += $row->SaldCtct;
            $data['acu_reca'] += $row->RecaCtct;
        }
        $data['acu_total'] = $data['acu_dere'] + $data['acu_reca'];
        $data['total_str'] = ucfirst(strtolower(NumeroALetras::convertir($data['acu_total'], '', 'centavos')));
        
        //comentar esta linea para hacer pruebas y no hacer inserts a la base de datos
        Boleto::insert_to_database($data, $boleto_i, $databoles_i);
        
        
        return $data;
    }
    
    
    
    public function codigoVerificadorXNET($entr_bar, $verif) 
    {
        $resultado = false;

        $entrada = "." . $entr_bar;     //porque php cuenta desde cero
                        //solo la cadena 

        $suma   = 0;
        $factor = array(11,13,17,19,23);
        $mod    = 100;
        $dv     = 0;
        $j      = 0;
        $l      = strlen($verif);

        for ($i = strlen($entrada)-1; $i > 0; $i--) {
            if(is_numeric($entrada[$i])) {
                $num = $entrada[$i] * $factor[$j];
            } else {
                $num = ord($entrada[$i]) * $factor[$j];
            }
            $suma = $suma + $num;
        $j = ++$j % count($factor);
        }

        $suma += 1000000;            //formateo el acumulador a 6 digitos.
        $d1 = substr($suma, -2);     //últimos '2' digitos del acumulador.
        $d2 = substr($suma, -4, 2);  //los '2' digitos del medio del acumulador.
        $d3 = abs($d1 - $d2);        //resta de ambos numeros SIN signo
        $dv = substr($d3+100, -$l);  //últimos 'l' digitos del calculo.

        // si lo llame con Verif_XNET ('123456789', NULL) --> devuelve el valor del verificador
        if ($verif == NULL) {
          $resultado = $dv;
        } else {
        // si lo llame con Verif_XNET ('123456789', '0' ) --> se fija si 0 es el valor del verificador calculado y devuelve true o false 

          // devuelvo true si el calculo me dio igual que lo que mandaron
          if ($dv == $verif)
            $resultado = true;
        }
        return $resultado;
    }
    
    
}
