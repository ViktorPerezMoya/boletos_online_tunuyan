<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Boleto extends Model
{
    //
    public static function all_conceptos(){
        //devuelve un array de objetos
        $conceptos = DB::select("SELECT c.CodiConc, c.DetaConc FROM recaudacion.concepto c WHERE c.PeriInfo = ? ORDER BY c.CodiConc ASC;",array(date('Y')) );
        return $conceptos;
    }
    
    public static function get_persona($cuenta,$tipo_bole){
        $sql = "";
        switch ($tipo_bole){
            case "inmueble":
                $sql = "SELECT p.DetaPers, p.CucuPers FROM recaudacion.inmueble i
                        INNER JOIN infogov.persona p ON p.CucuPers = i.CucuPers
                        WHERE i.CodiInmu = ?";
                break;
            case "comercio":
                $sql = "SELECT p.DetaPers, p.CucuPers FROM recaudacion.comercio c
                        INNER JOIN infogov.persona p ON p.CucuPers = c.CucuPers
                        WHERE c.CodiCome = ?";
                break;
            default :
                return null;
        }
        $result = DB::select($sql, array($cuenta));
        if(empty($result)){
            return null;
        }
        return $result[0];
    }
    
    public static function get_inmueble($cuenta,$tipo_bole){
        $sql = "";
        switch ($tipo_bole){
            case "inmueble":
                $sql = "SELECT i.CodiInmu, i.NomeInmu, i.DecpInmu, i.NupoInmu, i.DelpInmu FROM recaudacion.inmueble i
            WHERE i.CodiInmu = ?";
                break;
            case "comercio":
                $sql = "SELECT i.CodiInmu, i.NomeInmu, i.DecpInmu, i.NupoInmu, i.DelpInmu FROM recaudacion.comercio c
                    INNER JOIN recaudacion.inmueble i ON c.CodiInmu = i.CodiInmu
                    WHERE c.CodiCome = ?";
                break;
            default :
                return null;
        }
        
        $result = DB::select($sql, array($cuenta));
        if(empty($result)){
            return null;
        }
        return $result[0];
    }
    
    public static function get_oficina($tipo_bole){
        return DB::select("SELECT o.CodiOfic FROM recaudacion.oficina o WHERE o.DetaOfic = ?", [strtoupper($tipo_bole)])[0]->CodiOfic;
    }
    
    public static function get_boleto($cuenta,$tipo_bole){
        $join = "";
        switch ($tipo_bole){
            case "inmueble":
                $join = "INNER JOIN inmueble i ON i.CodiInmu = tmp.CuenCtct";
                break;
            case "comercio":
                $join = "INNER JOIN comercio i ON i.CodiCome = tmp.CuenCtct";
                break;
        }
        return DB::select("SELECT tmp.PeriBole, tmp.CodiOfic, tmp.CuenCtct, i.CucuPers FROM tmp_ctacteboleto tmp "
                .$join. " WHERE tmp.CuenCtct = $cuenta AND tmp.PeriBole = ".date('Y')." LIMIT 1;")[0];
    }
    
    public static function get_data_bole($codiOfic,$cuenta,$arr_peridos){
        return DB::select("SELECT tmp.PeriBole, tmp.PeriCtct, tmp.BimeCtct, tmp.NumeCtct, tmp.MoviCtct,tmp.CodiOfic,tmp.CuenCtct,
        tmp.CodiFapa, tmp.CuotDefa, tmp.NumeApre, tmp.CodiTimo, tmp.PeriInfo, tmp.CodiConc, tmp.SaldCtct, tmp.RecaCtct
        FROM tmp_ctacteboleto tmp
        INNER JOIN ctacte cc ON cc.CodiOfic = tmp.CodiOfic AND cc.CuenCtct = tmp.CuenCtct AND cc.PeriCtct = tmp.PeriCtct AND cc.BimeCtct = tmp.BimeCtct 
        AND cc.NumeCtct = tmp.NumeCtct AND cc.MoviCtct = tmp.MoviCtct
        WHERE tmp.CodiOfic = $codiOfic AND tmp.CuenCtct = $cuenta AND tmp.PeriCtct = ".date('Y')." AND tmp.BimeCtct IN ($arr_peridos)");
        
    }
    
    public static function insert_to_database($data,$boleto_i, $databoles_i){
        
        DB::beginTransaction();
        try{
            DB::table('boleto')->insert(array(
                'PeriBole' => $boleto_i->PeriBole,
                'NumeBole' => $data['numeBole'],
                'CodiOfic' => $boleto_i->CodiOfic,
                'CuenCtct' => $boleto_i->CuenCtct,
                'CucuPers' => $boleto_i->CucuPers,
                'FealBole' => date('Y-m-d H:i:s'),
                'FeveBole' => date('Y-m-d H:i:s'),
                'CodiEnre' => 0,
                'TipoBole' => 'Mensual',
                'EstaBole' => 'Emitido',
                'FeanBole' => null,
                'MoanBole' => '',
                'AnulUsua' => '',
                'CodiTili' => 0,
                'NumeLiqu' => 0,
                'CodiFapa' => 0,
                'CuotDefa' => 0,
                'ExpeBole' => '',
                'ObseBole' => '',
                'CapiBole' => $data['acu_dere'],
                'RecaBole' => $data['acu_reca'],
                'DecaBole' => 0,
                'DereBole' => 0,
                'InteBole' => 0,
                'ExenBole' => 0,
                'CrafBole' => 0,
                'ApreBole' => 0,
                'TotaBole' => ($data['acu_dere'] + $data['acu_reca']),
                'CodiUsua' => 'Web'
            ));
            
            foreach ($databoles_i as $row){
                    DB::table('detabole')->insert(array(
                    'PeriBole' => $row->PeriBole,
                    'NumeBole' => $data['numeBole'],
                    'PeriCtct' => $row->PeriCtct,
                    'BimeCtct' => $row->BimeCtct,
                    'NumeCtct' => $row->NumeCtct,
                    'MoviCtct' => $row->MoviCtct,
                    'MoviDebo' => 1,
                    'CodiTili' => 0,
                    'NumeLiqu' => 0,
                    'CodiOfic' => $row->CodiOfic,
                    'CuenCtct' => $row->CuenCtct,
                    'TipoCtct' => "Mensual",
                    'CodiFapa' => $row->CodiFapa, 
                    'CuotDefa' => $row->CuotDefa, 
                    'NumeApre' => $row->NumeApre, 
                    'CodiTimo' => $row->CodiTimo, 
                    'PeriInfo' => $row->PeriInfo, 
                    'CodiConc' => $row->CodiConc, 
                    'CantDebo' => 0, 
                    'ObseDebo' => NULL, 
                    'CapiDebo' => $row->SaldCtct, 
                    'DecaDebo' => 0.00, 
                    'RecaDebo' => $row->RecaCtct, 
                    'DereDebo' => 0.00, 
                    'InteDebo' => 0.00, 
                    'ExenDebo' => 0.00, 
                    'CrafDebo' => 0.00, 
                    'ApreDebo' => 0.00, 
                    'TotaDebo' => ($row->SaldCtct + $row->RecaCtct)
                ));
            }
            
           DB::commit(); 
        } catch (Exception $ex) {
            DB::rollback();
        }
    }
    
    
    public static function createTableTmp($tipo_boleto, $cuenta){
        $tipo_int = Boleto::getTypeBoleto($tipo_boleto);
        DB::statement(DB::raw("CALL fnDeuda(?, ?, NOW())"), [$tipo_int."", $cuenta.""]);
    }
    
    public static function getTypeBoleto($tipo_boleto){
            //1 – Inmueble, 2 – Comercio, 3 – Deudores Varios, 4 – Cementerio 5 – Transito
            switch (strtolower(trim($tipo_boleto))){
                case 'inmueble':
                    return 1;
                case 'comercio':
                    return 2;
                default :
                    return 0;
            }
    }
}
