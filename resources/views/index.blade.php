@extends('layouts.app')

@section('content')
<div class="container">
    <br>
    <a href="{{ url('/') }}" class="float-right">Inicio</a>
    <h1>Boletos Online</h1>
    <p>Cuenta: <small>{{ $cuenta }}</small></p>
    <div class="form-inline">
        <div class="form-group" style="margin-right: 10px">
            <label for="tipo_boleto" style="margin-right: 10px">Sector: </label>
            <select class="form-control" name="tipo_boleto" id="tipo_boleto">
                <option selected>Seleccione Sector...</option>
                <option value="inmueble" {{ (!empty($tipoboleto) && $tipoboleto == 'inmueble' ? 'selected' : '') }}>Inmueble</option>
                <option value="comercio" {{ (!empty($tipoboleto) && $tipoboleto == 'comercio' ? 'selected' : '') }}>Comercio</option>
            </select>
        </div>
        <div class="form-group" style="margin-right: 10px">
            <label for='cuenta' style="margin-right: 10px">Ingrese número de cuenta: </label>
            <input type="number" id="cuenta" name="cuenta" class="form-control" value="{{ ($cuenta == '0000'? '' : $cuenta) }}" placeholder="Ingrese número de cuenta...">
        </div>
        <button type="submit" class="btn btn-primary" id="btn_consultar_deuda">Consultar</button>
    </div>
    <hr>
    @if(empty($boletos))
        @include('info')
    @else
        @include('boletosTable')
    @endif
    
</div>
@endsection