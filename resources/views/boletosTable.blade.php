<div class="row">
        <div class="col-md-12">
            <p class="float-left">
                Total: <span id="total_cobrar">$0.00 </span>&nbsp;&nbsp;
                Total Recargo: <span id="total_recargo_cobrar">$0.00</span>
            </p>
            <button  type="button"class="btn btn-primary float-right" id="btn_print">
                Imprimir
            </button>
            <div class="clear-fix"></div>
            <br><br>
            <table class="table table-sm table-striped table-hover table-responsive-sm" id="table_boletos">
                <thead>
                    <tr>
                        <th>Periodo</th>
                        <th>Bime/Mens</th>
                        <th>Concepto</th>
                        <th>Vencimiento</th>
                        <th>Capital</th>
                        <th>Recargo</th>
                        <th>Total</th>
                        <th style="display: none;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($boletos as $boleto)
                    <tr class="data">
                        <td>
                            <input type="hidden" name="nume_ctct" value="{{ $boleto->NumeCtct }}">
                            <input type="hidden" name="movi_ctct" value="{{ $boleto->MoviCtct }}">
                            {{ $boleto->PeriCtct }}
                        </td>
                        <td class="numbimestre">{{ $boleto->BimeCtct }}</td>
                        <td>{{ $boleto->DetaConc }}</td>
                        <td>{{ date_format(date_create($boleto->FeveCtct),'d-m-Y') }}</td>
                        <td>{{ $boleto->SaldCtct }}</td>
                        <td class="total_rec">{{ $boleto->RecaCtct }}</td>
                        <td class="total_row">{{ ($boleto->SaldCtct + $boleto->RecaCtct) }}</td>
                        <td><input type="checkbox" class="checked_bime {{ 'bime'.$boleto->BimeCtct }}"></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="paging-container" id="pagination_boletos"> </div>

        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="modalPrintBoleto" tabindex="-1" role="dialog" aria-labelledby="tituloBoleto"  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="btn_close_modal"><a href="#" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ url('/img/close_icon.png') }}"></a></div>
          
    <div class="modal-content">
      <div class="modal-body boleto_modal">
          <div id="div_boleto_pdf" class="loader">
          </div>
      </div>
    </div>
  </div>
</div>
