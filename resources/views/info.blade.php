<br><br>
<div class="row">
        <div class="col-md-12">
            <div class="float-right">
                <img class="img-fluid" src="{{ url('/img/persona.png') }}">
            </div>
            <div class="card text-white bg-secondary mb-3 float-right">
                <div class="card-header">¿Como obtengo mis boletos de pago?</div>
                <div class="card-body">
                  <h5 class="card-title">Los pasos son muy sencillos: </h5>
                  <p class="card-text" style="width: 300px;">Ingrese su número de cuenta y posteriormente, en la lista desplegable, seleccione el tipo de boleto que quiere consultar. Luego haga clic en el boton "Consultar" y listo!!</p>
                  <p>
                    <small>Los boletos impresos son correspondientes al periodo actual.</small>
                  </p>
                </div>
              </div>
        </div>
    </div>