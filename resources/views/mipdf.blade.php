<!DOCTYPE hmtl>
<html>
    <head>
        <title>Boleto</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <style>
            .tabla_footer{
                position: absolute;
                /*bottom: 110px;*/
            }
            .tabla_footer tr.tbf_header{
                text-align: center;
                background-color: #cccccc;
            }
            .tabla_footer tr.tbf_title,tr.tbf_code{
                text-align: center;
            }
            
            .tabla, .tabla_footer{
                width: 100%;font-family: Arial, Helvetica, sans-serif;font-size: 10px;border-collapse: collapse;
            }
            .tabla td.head_label{
                width: 50px;text-align: right;background-color: #cccccc;
            }
            .image_head{
                float: left;height: 100px;width: 150px;
            }
            .titulo_bole{
                width: 80%;text-align: center;font-size: 11px;
                font-family: Arial, Helvetica, sans-serif;
            }
            .titulo_bole strong{
                font-size: 12px;
            }
            td.cabecera_bimestre{
                text-align: right;
            }
            .tabla td, .tabla_footer td{
                border: 1px solid black;
            }
            .subtabla{
                width: 100%;text-align: right;
            }
            .subtabla td{
                border: none;
            }
            td.totales{
                background-color: #cccccc;
                text-align: right;
            }
            .detalles{
                font-size: 11px;font-family: Arial, Helvetica, sans-serif;

            }
            .cel_monto{
                text-align: right;
            }
            .barcode{
            }
        </style>
    </head>
    <body>
        <img src="{{ url('/img/logo_tunuyan.jpg') }}" class="image_head">
        <p class="titulo_bole">
            <strong>Municipalidad de Tunuyán</strong><br>
            Departamento de Rentas<br>
            República de Siria y Alem - Tunuyán - Mendoza<br>
            Tel./Fax. (02622)-422195/193<br>
            Tel. de Atención al Vecino: 08002220377 - CUIT 30-99916956-9 <br>
        </p>
        <br><br>
        <hr>
        <table class="tabla">
            <tr>
                <td class="head_label">Concepto:</td>
                <td>EMISIÓN DE DEUDA - {{ strtoupper($tipobole) }}</td>
                <td class="head_label">Boleto:</td>
                <td>
                    <table class="subtabla">
                        <tr>
                            <td>{{ $boleto->PeriBole.'/'.$numeBole }}</td>
                            <td style="background-color: #cccccc;border-color: #cccccc;">Vencimiento: </td>
                            <td>{{ $fechaactual }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="head_label">Datos del contribuyente:</td>
                <td>
                    {{ (empty($persona->DetaPers) ? '' : $persona->DetaPers) }}
                    <br>
                    {{ (empty($persona->CucuPers) ? '' : $persona->CucuPers) }}
                </td>
                <td class="head_label">Datos del objeto imponible:</td>
                <td>
                    REFERENCIA  : {{ (empty($inmueble) ? '' : $inmueble->CodiInmu) }}<br>
                    NOMENCLATURA: {{ (empty($inmueble) ? '' : $inmueble->NomeInmu) }}<br>
                    DOM.    REAL: {{ (empty($inmueble) ? '' : $inmueble->DecpInmu.' '.$inmueble->NupoInmu.' '.$inmueble->DelpInmu) }}
                </td>
            </tr>
        </table>
        <table class="tabla">
            <tr style="background-color: #cccccc;text-align: center;">
                <td>PERIODO</td>
                <td>BIMESTRE</td>
                <td>DESCRIPCIÓN</td>
                <td>DERECHO</td>
                <td>RECARGO</td>
                <td>INTERES</td>
                <td>DESCUENTO</td>
                <td>TOTAL</td>
            </tr>
            <!-- bimestre -->
            @foreach($conceptos as $concepto)
                <?php
                $cont = 0;
                $first = false;
                ?>
                @foreach($databole as $data)
                    @if($data->CodiConc == $concepto->CodiConc)
                    <?php $cont++;?>
                    @endif
                @endforeach
                
                @if($cont > 0)
                    <tr>
                        <td colspan="8">
                            {{ $concepto->CodiConc }} - {{ $concepto->DetaConc }}
                        </td>
                    </tr>
                @endif
                
                @foreach($databole as $data)
                    @if($data->CodiConc == $concepto->CodiConc)
                    
                    
                    <tr>
                        @if(!$first)
                        <td rowspan="{{ $cont }}" class="cabecera_bimestre">{{ $data->PeriCtct }}</td>
                        <?php $first = true; ?>
                        @endif
                        <td>{{ $data->BimeCtct }}</td>
                        <td></td>
                        <td class="cel_monto">{{ number_format($data->SaldCtct,2,',','.') }}</td>
                        <td class="cel_monto">{{ number_format($data->RecaCtct,2,',','.') }}</td>
                        <td></td>
                        <td></td>
                        <td class="cel_monto">{{ number_format(($data->SaldCtct + $data->RecaCtct),2,',','.') }}</td>
                       
                    </tr>
                    @endif
                @endforeach
            @endforeach
            <tr>
                <td></td>
                <td class="totales" colspan="2">Totales: </td>
                <td class="totales">{{ number_format($acu_dere,2,',','.') }}</td>
                <td class="totales">{{ number_format($acu_reca,2,',','.') }}</td>
                <td class="totales">00,00</td>
                <td class="totales">00,00</td>
                <td class="totales">{{ number_format($acu_total,2,',','.') }}</td>
            </tr>
            <tr>
                <td colspan="8">
                    SON PESOS: <?php echo $total_str ?>.---
                </td>
            </tr>
        </table>
        <p class="detalles">
            Los recargos por pago fuera de término se incluirán en la próxima facturación.<br>
            <strong>El pago de sus tasas vuelve a usted en obras y servicios.<br>
            Lugares habilitados para el pago: Banco Nación, Banco Credicoop, Banco Superville, Nevada, Pago Facil,<br>
            Rapipago, Montemar Cia. Financiera, Tesorería Municipal, Red Link, Red Banelco y PagoMisCuentas.com.<br>
            Solo acredita pago con el ticket correspondiente.<br>
            Según Ord. 2359/11 se incluye 3 % de aporte voluntario a Bomberos.</strong>

        </p>
        <!--<div class="footer">-->
            <table class="tabla_footer">
                <tr class="tbf_title">
                    <td colspan="8">
                        <strong>MUNICIPALIDAD DE TUNUYÁN - Departamento de Rentas</strong>
                    </td>
                </tr>
                <tr class="tbf_header">
                    <td colspan="8">
                        APELLIDO Y NOMBRE
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        CAFRATTI ALEJANDRA VERONICA
                    </td>
                </tr>
                <tr class="tbf_header">
                    <td>CUOTA</td>
                    <td>FACILIDAD</td>
                    <td>USUARIO</td>
                    <td>OFICINA</td>
                    <td>CUENTA</td>
                    <td>BOLETO</td>
                    <td>VENCIMIENTO</td>
                    <td>IMPORTE</td>
                </tr>
                <tr>
                    <td class="cel_monto">0</td>
                    <td class="cel_monto">0</td>
                    <td>Web</td>
                    <td class="cel_monto">1</td>
                    <td class="cel_monto">{{ $cuenta }}</td>
                    <td>{{ $boleto->PeriBole }}/{{ $numeBole }}</td>
                    <td>{{ $fechaactual }}</td>
                    <td class="cel_monto">{{ number_format($acu_total,2,',','.') }}</td>
                </tr>
                <tr class="tbf_code">
                    <td colspan="8" style="text-align: center;">
                        {!! DNS1D::getBarcodeHTML($codebar_str, "C128",1) !!}
                        {{ $codebar_str }}
                    </td>
                </tr>
            </table>
        <!--</div>-->
    </body>
</html>