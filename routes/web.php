<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
 */
 
Route::get('/',function () {
    return view('index',array('cuenta' => '0000', 'boletos' => null));
});

Route::get('/boletos/{tipo_boleto}/{cuenta}',array(
    'as' => 'resultBoleto',
    'uses' => 'IndexController@result'
));

Route::get('/hola-mundo', function(){
	return "Hola mundo desde una route";
});

Route::get('generar-boleto/{cuenta}/{tipo_bole}/{bimestres}','IndexController@generarBoleto');

//Route::post('/boletos_bimestre',array(
//    'as'=>'boletosBimestre',
//    'uses' => 'IndexController@boletosBimestre'
//));


