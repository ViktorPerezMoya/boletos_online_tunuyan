//var URL_BASE = "https://infogov.com.ar/proveedores/tunuyan/boletos_online";
var URL_BASE = "http://localhost/boletos_online_tunuyan/public";
var cuenta = "";
var tipo_boleto = "";
var arr_bimestres = new Array();
var total = 0.0;
var total_regargo = 0.0;
$(document).ready(function () {
    

    $("#btn_consultar_deuda").click(function () {
        consultar_deuda();
    });
    
//    $(".checked_bime").change(function(){
//        var fila = $(this).parents('tr');
//        var mes = fila.find("td[class=numbimestre]").html();
//        
//        if($(this).prop("checked")){
//            seleccionarBoletos(mes);
//            actualizarTotal(this,"sumar");
//            pintarFila(this,true);
//        }else{
//            deseleccionarBoletos(mes);
//            actualizarTotal(this,"restar");
//            pintarFila(this,false);
//        }
//    });
    
    $("tr").click(function(){
        var fila = $(this);
        var mes = fila.find("td[class=numbimestre]").html();
        var check = fila.find("input[type=checkbox]");
        if(check.prop("checked")){
            check.prop("checked",false);
        }else{
            check.prop("checked",true);
        }
        
        if(check.prop("checked")){
            seleccionarBoletos(mes);
            actualizarTotal(this,"sumar");
            pintarFila(this,true);
        }else{
            deseleccionarBoletos(mes);
            actualizarTotal(this,"restar");
            pintarFila(this,false);
        }
    });
    
    $("#btn_print").click(function(){
        if(arr_bimestres.length > 0){
            cuenta = $("#cuenta").val();
            tipo_boleto = $("#tipo_boleto").val();
            var str_bimestres = "";
            for(var i = 0; i < arr_bimestres.length; i++){
                if(i < arr_bimestres.length-1){
                    str_bimestres += arr_bimestres[i]+"-";
                }else{
                    str_bimestres += arr_bimestres[i];
                }
            }
           
            if($(window).width() > 728){ //es pantalla de ordenador
                $("#div_boleto_pdf").html(
                '<embed id="pdf_object" type="application/pdf" src="'+URL_BASE+'/generar-boleto/'+cuenta+'/'+tipo_boleto+'/'+str_bimestres+'" style="width:100%;height:500px"  />'    
                );

                $('#modalPrintBoleto').modal('show');
            }else{
                window.open(URL_BASE+'/generar-boleto/'+cuenta+'/'+tipo_boleto+'/'+str_bimestres);
            }
        }else{
            alert("Error: No hay bimestres seleccionados.");
        }
    });

    $("#cuenta").keypress(function(e){
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            consultar_deuda();
        }
    });
    
    /**PAGINACION DE BOLETOS**/
    new Pagination('#pagination_boletos', {
        itemsCount: $("#table_boletos tr").length,
        onPageSizeChange: function (ps) {
            console.log('changed to ' + ps);
        },
        onPageChange: function (paging) {
            //custom paging logic here
            var start = paging.pageSize * (paging.currentPage - 1),
                    end = start + paging.pageSize,
                    $rows = $('#table_boletos').find('.data');

            $rows.hide();

            for (var i = start; i < end; i++) {
                $rows.eq(i).show();
            }
        }
    });

});

function consultar_deuda(){
    cuenta = $("#cuenta").val();
        tipo_boleto = $("#tipo_boleto").val();
        window.location.href = URL_BASE + "/boletos/" + tipo_boleto + "/" + cuenta;
}

function seleccionarBoletos(bime){
    arr_bimestres.push(parseInt(bime));
    selectCheck(bime,true);
}
function deseleccionarBoletos(bime){
    
    selectCheck(bime,false);
    
    var index = arr_bimestres.findIndex(function (element){
        return element == parseInt(bime);
    });
    
    arr_bimestres.splice(index,1);
    
}

function selectCheck(nbime,value){
//    console.log(nbime);
    $(".bime"+nbime).prop("checked",value);
}

function actualizarTotal(context,operacion){
    var fila = $(context);//.parents('tr');
    var bime = $.trim(fila.find("td[class=numbimestre]").html());
    var total_row = $.trim(fila.find("td[class=total_row]").html());
    var total_rec = $.trim(fila.find("td[class=total_rec]").html());
    cambiarTotal(parseFloat(total_row),parseFloat(total_rec), operacion);
    
    var brothers = fila.siblings();
    for(var i = 0; i < brothers.length; i++){
        var bime_br = brothers[i].children[1].innerHTML;
        if( bime_br == bime){
            var total_rec_br = brothers[i].children[5].innerHTML;
            var total_br = brothers[i].children[6].innerHTML;
            cambiarTotal(parseFloat(total_br),parseFloat(total_rec_br), operacion);
        }
    }
    
}

function cambiarTotal(importe, recargo, operacion){
    switch (operacion){
        case "sumar":
            total = total + importe;
            total_regargo = total_regargo + recargo;
            break;
        case "restar":
            total = total - importe;
            total_regargo = total_regargo - recargo;
            break;
    }
    $("#total_cobrar").text("$"+total.toFixed(2));
    $("#total_recargo_cobrar").text("$"+total_regargo.toFixed(2));
}

function pintarFila(context, pintar){
    var fila = $(context);//.parents("tr");
    var bime = $.trim(fila.find("td[class=numbimestre]").html());
    
    if(pintar){
        fila.addClass("selected_row");
    }else{
        fila.removeClass("selected_row");
    }
    
    //pintamos todos los que pertenezcan al mismo bimestre
    var brothers = fila.siblings();
    for(var i = 0; i < brothers.length; i++){
        var bime_br = brothers[i].children[1].innerHTML;
        if( bime_br == bime){
            if(pintar){
                brothers[i].className = "data selected_row";
            }else{
                brothers[i].className = "data";
            }
        }
    }
}